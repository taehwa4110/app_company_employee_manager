import 'package:flutter/material.dart';

class ComponentMyInfo extends StatelessWidget {
  const ComponentMyInfo({super.key, required this.myInfoKind, required this.contents});
  final String myInfoKind;
  final String contents;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          decoration: BoxDecoration(
              border:Border.all(color: Colors.black),
              color: Colors.grey
          ),
          width: 100,
          height: 40,
          child: Text(myInfoKind, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
        ),
        Container(
          decoration: BoxDecoration(
              border:Border.all(color: Colors.black),
          ),
          width: 250,
          height: 40,
          child: Text(contents, style: TextStyle(fontSize: 20),),
        )
      ],
    );
  }
}
