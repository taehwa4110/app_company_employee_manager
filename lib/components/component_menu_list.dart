import 'package:flutter/material.dart';

class ComponentMenuList extends StatelessWidget {
  const ComponentMenuList({super.key, required this.menuName, required this.icon, required this.callback});
  
  final String menuName;
  final IconData icon;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child:Column(
        children: [
          Text(menuName, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, height: 2)),
          Icon(icon, size: 95, color: Colors.grey,)
        ],
      ) ,
      onTap: callback,
    );
  }
}
