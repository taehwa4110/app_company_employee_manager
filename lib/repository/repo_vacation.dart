import 'package:app_company_employee_manager/config/config_api.dart';
import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/model/common_result.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_create_request.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_cunt_detail_single_result.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_list_response.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_my_list_item_result.dart';
import 'package:dio/dio.dart';

class RepoVacation {
  Future<CommonResult> setData(VacationCreateRequest vacationCreateRequest) async {
    String baseUrl = '$apiUri/vacation/new/vacation-history/{employeeId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.post(
        baseUrl.replaceAll('{employeeId}', token!),
        data: vacationCreateRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return CommonResult.fromJson(response.data);
  }

  Future<VacationListResponse> getList() async {
    String baseUrl = '$apiUri/vacation/vacation-list/{employeeId}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{employeeId}', token!),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              if (status == 200) {
                return true;
              } else {
                return false;
              }
            }));

    return VacationListResponse.fromJson(response.data);
  }

  Future<CommonResult> delData(String dateHoliday) async {
    String baseUrl = '/v1/member/holiday-my/date/{dateHoliday}';

    Dio dio = Dio();

    final response =
        await dio.delete(baseUrl.replaceAll('{dateHoliday}', dateHoliday),
            options: Options(
                followRedirects: false,
                validateStatus: (status) {
                  if (status == 200) {
                    return true;
                  } else {
                    return false;
                  }
                }));

    return CommonResult.fromJson(response.data);
  }

  Future<VacationMyListItemResult> getMyVacation(int year, int month) async {
    String baseUrl = '$apiUri/vacation/vacation-my-list/{employeeId}/{year}/{month}';

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.get(
      baseUrl.replaceAll('{employeeId}', token!).replaceAll('{year}', year.toString()).replaceAll('{month}', month.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return VacationMyListItemResult.fromJson(response.data);
  }

  Future<VacationCountDetailSingleResult> getMyVacationCount() async {
    String baseUrl = '$apiUri/vacation/vacation-count/{employeeId}';
    
    Dio dio = Dio();
    
    String? token = await TokenLib.getToken();
    
    final response = await dio.get(
        baseUrl.replaceAll('{employeeId}', token!),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return VacationCountDetailSingleResult.fromJson(response.data);
  }
}
