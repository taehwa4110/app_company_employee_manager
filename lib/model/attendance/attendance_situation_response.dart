class AttendanceSituationResponse {
  String attendanceSituation;
  String timeCompanyIn;
  String timeCompanyOut;
  String timeEarlyLeave;

  AttendanceSituationResponse(this.attendanceSituation, this.timeCompanyIn, this.timeCompanyOut, this.timeEarlyLeave);

  factory AttendanceSituationResponse.fromJson(Map<String, dynamic> json) {
    return AttendanceSituationResponse(
      json['attendanceSituation'],
      json['timeCompanyIn'],
      json['timeCompanyOut'],
      json['timeEarlyLeave'],
    );
  }
}