  import 'package:app_company_employee_manager/model/vacation/vacation_list_item.dart';

class VacationListResponse {
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<VacationListItem>? list;

  VacationListResponse(
      this.totalItemCount,
      this.totalPage,
      this.currentPage, {
        this.list,
      });

  factory VacationListResponse.fromJson(Map<String, dynamic> json) {
    return VacationListResponse(
      json['totalItemCount'],
      json['totalPage'],
      json['currentPage'],
      list: json['list'] != null ? (json['list'] as List).map((e) => VacationListItem.fromJson(e)).toList() : null,
    );
  }
}
