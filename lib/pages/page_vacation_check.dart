import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_count_title.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_vacation_list.dart';
import 'package:app_company_employee_manager/config/config_dropdown.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_my_list_item.dart';
import 'package:app_company_employee_manager/pages/page_attendance.dart';
import 'package:app_company_employee_manager/pages/page_attendance_check.dart';
import 'package:app_company_employee_manager/pages/page_menu.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class PageVacationCheck extends StatefulWidget {
  const PageVacationCheck({Key? key}) : super(key: key);

  @override
  State<PageVacationCheck> createState() => _PageVacationCheckState();
}

class _PageVacationCheckState extends State<PageVacationCheck> {
  int _selectYear = DateTime.now().year;
  int _selectMonth = DateTime.now().month;
  String _totalVacationCount = '-';
  String _usageVacationCount = '-';
  String _leaveVacationCount = '-';

  List<VacationMyListItem> _list = [];
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _getMyVacationList();
    _getMyVacationCount();
  }

  Future<void> _getMyVacationList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().getMyVacation(_selectYear, _selectMonth).then((res)  {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _getMyVacationCount() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().getMyVacationCount().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _totalVacationCount = res.data.totalVacationCount;
        _usageVacationCount = res.data.usageVacationCount;
        _leaveVacationCount = res.data.leaveVacationCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
          title: '휴가 조회'
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody()
        ],
      ),
      bottomNavigationBar: BottomAppBar(
          color: Colors.blue,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageMenu())
                      );
                    },
                    icon: Icon(Icons.home, )
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendance())
                      );
                    },
                    icon: Icon(Icons.watch_later)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                      );
                    },
                    icon: Icon(Icons.calendar_today)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationApplication())
                      );
                    },
                    icon: Icon(Icons.event)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationCheck())
                      );
                    },
                    icon: Icon(Icons.content_paste_search_outlined, color: Colors.white,)
                ),
              ],
            ),
          )
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 20,),
          _buildForm(),
          SizedBox(height: 20,),
          DataTable(
              decoration:BoxDecoration(
                  border: Border.all(color: Colors.black)
              ) ,
              columns: [
                DataColumn(label: Text('총 연차수')),
                DataColumn(label: Text('사용 연차수')),
                DataColumn(label: Text('남은 연차수')),
              ],
              rows: [
                DataRow(
                    cells: [
                      DataCell(Text(_totalVacationCount)),
                      DataCell(Text(_usageVacationCount)),
                      DataCell(Text(_leaveVacationCount)),
                    ]
                ),
              ]
          ),
          SizedBox(height: 20,),
          ComponentCountTitle(icon: Icons.list, count: _list.length, unitName: '건', itemName: '데이터'),
          Container(
            height: 50,
            decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('날짜'),
                Text('휴가 제목'),
                Text('상태'),
              ],
            ),
          ),
          ListView.builder(
              padding: const EdgeInsets.all(8),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) =>
                  ComponentVacationList(item: _list[index]))
        ],
      ),
    );
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      child: Row(
        children: [
          DropdownButtonHideUnderline(
            child: DropdownButton2(
              isExpanded: true,
              hint: Row(
                children: [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: Colors.brown,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      _selectYear.toString(),
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
              items: dropdownYear,
              onChanged: (year) {
                setState(() {
                  _selectYear = year!;
                  _getMyVacationList();
                });
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              iconSize: 14,
              iconEnabledColor: Colors.indigo,
              iconDisabledColor: Colors.grey,
              buttonHeight: 50,
              buttonWidth: 160,
              buttonPadding: const EdgeInsets.only(left: 14, right: 14),
              buttonDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                border: Border.all(
                  color: Colors.black26,
                ),
                color: Colors.blueAccent,
              ),
              buttonElevation: 2,
              itemHeight: 40,
              itemPadding: const EdgeInsets.only(left: 14, right: 14),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownPadding: null,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: Colors.blueAccent,
              ),
              dropdownElevation: 8,
              scrollbarRadius: const Radius.circular(40),
              scrollbarThickness: 6,
              scrollbarAlwaysShow: true,
              offset: const Offset(-20, 0),
            ),
          ),
          Text('년'),
          SizedBox(width: 20,),
          DropdownButtonHideUnderline(
            child: DropdownButton2(
              isExpanded: true,
              hint: Row(
                children: [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: Colors.brown,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      _selectMonth.toString(),
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
              items: dropdownMonth,
              onChanged: (month) {
                setState(() {
                  _selectMonth = month!;
                  _getMyVacationList();
                });
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              iconSize: 14,
              iconEnabledColor: Colors.indigo,
              iconDisabledColor: Colors.grey,
              buttonHeight: 50,
              buttonWidth: 160,
              buttonPadding: const EdgeInsets.only(left: 14, right: 14),
              buttonDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                border: Border.all(
                  color: Colors.black26,
                ),
                color: Colors.blueAccent,
              ),
              buttonElevation: 2,
              itemHeight: 40,
              itemPadding: const EdgeInsets.only(left: 14, right: 14),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownPadding: null,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: Colors.blueAccent,
              ),
              dropdownElevation: 8,
              scrollbarRadius: const Radius.circular(40),
              scrollbarThickness: 6,
              scrollbarAlwaysShow: true,
              offset: const Offset(-20, 0),
            ),
          ),
          Text('월'),

        ],
      ),

    );
  }
}

