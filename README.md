### 사원 관리 APP
***
(개인프로젝트) 사원 스스로 근태, 휴가 등을 관리하는 APP
***

### Language
```
Flutter 3.3.2
Dart 2.18.1
```

### 기능
```
*로그인

* 메인페이지
 - 목록

* 마이페이지
 - 내 정보 조회
 - 비밀번호 변경
 - 로그아웃
 
* 근태 등록
 - 출근, 조퇴, 퇴근
 
* 근태 조회
 - 특정 년/달에 내 근태 리스트 조회
 
* 휴가 신청

* 휴가 조회
  - 특정 년/달에 내 휴가 리스트 조회 및 나의 연차 개수 조회 
```

### 앱 화면
>* 로그인 화면
>
>![app_login](./images/app-login.jpg)

>* 메인 화면
>
>![app_menu](./images/app-menu.jpg)

>* 목록
>
>![app_menu_list](./images/app-menu-list.jpg)

>* 마이페이지
>
>![app_my_info](./images/app-my-info.jpg)

>* 비밀번호 변경
>
>![app_password_change](./images/app-password-change.jpg)

>* 근태 등록
>
>![app_attendance-none](./images/app-attendance-none.jpg) 
>![app_attendance](./images/app-attendance-company-in.jpg)
>![app_attendance](./images/app-attendance-company-out.jpg)

>* 근태 조회
>
>![app_attendance_check](./images/app-attendance-check.jpg)

>* 휴가 신청
>
>![app_vacation_application](./images/app-vacation-application.jpg)

>* 휴가 조회
>
>![app_vacation_check](./images/app-vacation-check.jpg)

