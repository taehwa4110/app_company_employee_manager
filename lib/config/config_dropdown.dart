import 'package:flutter/material.dart';

const List<DropdownMenuItem<String>> dropdownVacationType = [
  DropdownMenuItem(value: 'ANNUAL', child: Text('연차')),
  DropdownMenuItem(value: 'HALFWAY', child: Text('반차')),
];

 List<DropdownMenuItem<int>> dropdownYear = [
   for(int year = DateTime.now().year -5; year <= DateTime.now().year + 5; year++)
  DropdownMenuItem(value: year, child: Text('$year')),
];

 List<DropdownMenuItem<int>> dropdownMonth = [
  for(int month = 1; month <= 12; month++)
    DropdownMenuItem(value: month, child: Text('$month')),
];
