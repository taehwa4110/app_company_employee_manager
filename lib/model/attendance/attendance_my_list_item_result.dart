import 'package:app_company_employee_manager/model/attendance/attendance_my_list_item.dart';

class AttendanceMyListItemResult {
  bool isSuccess;
  int code;
  String msg;
  int totalPage;
  int totalItemCount;
  int currentPage;
  List<AttendanceMyListItem> list;

  AttendanceMyListItemResult(this.isSuccess, this.code, this.msg, this.totalPage, this.totalItemCount, this.currentPage, this.list);

  factory AttendanceMyListItemResult.fromJson(Map<String, dynamic> json) {
    return AttendanceMyListItemResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      json['totalPage'],
      json['totalItemCount'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => AttendanceMyListItem.fromJson(e)).toList(),
    );
  }
}