import 'package:app_company_employee_manager/model/vacation/vacation_count_detail.dart';

class VacationCountDetailSingleResult {
  bool isSuccess;
  int code;
  String msg;
  VacationCountDetail data;

  VacationCountDetailSingleResult(this.isSuccess, this.code, this.msg, this.data);

  factory VacationCountDetailSingleResult.fromJson(Map<String, dynamic> json) {
    return VacationCountDetailSingleResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      VacationCountDetail.fromJson(json['data']),
    );
  }
}