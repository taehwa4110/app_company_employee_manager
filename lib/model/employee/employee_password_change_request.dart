class EmployeePasswordChangeRequest {
  String currentPassword;
  String changePassword;
  String checkChangePassword;

  EmployeePasswordChangeRequest(this.currentPassword, this.changePassword, this.checkChangePassword);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['currentPassword'] = this.currentPassword;
    data['changePassword'] = this.changePassword;
    data['checkChangePassword'] = this.checkChangePassword;

    return data;
  }
}