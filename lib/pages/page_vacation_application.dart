import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/config/config_dropdown.dart';
import 'package:app_company_employee_manager/config/config_form_validator.dart';
import 'package:app_company_employee_manager/model/vacation/vacation_create_request.dart';
import 'package:app_company_employee_manager/pages/page_attendance.dart';
import 'package:app_company_employee_manager/pages/page_attendance_check.dart';
import 'package:app_company_employee_manager/pages/page_menu.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:app_company_employee_manager/repository/repo_vacation.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class PageVacationApplication extends StatefulWidget {
  const PageVacationApplication({Key? key}) : super(key: key);

  @override
  State<PageVacationApplication> createState() => _PageVacationApplicationState();
}

class _PageVacationApplicationState extends State<PageVacationApplication> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVacation(VacationCreateRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoVacation().setData(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '휴가 등록 성공',
          subTitle: res.msg
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '휴가 등록 실패',
          subTitle: '휴가 등록에 실패하였습니다'
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '휴가 신청'
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomAppBar(
          color: Colors.blue,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageMenu())
                      );
                    },
                    icon: Icon(Icons.home,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendance())
                      );
                    },
                    icon: Icon(Icons.watch_later)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                      );
                    },
                    icon: Icon(Icons.calendar_today)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationApplication())
                      );
                    },
                    icon: Icon(Icons.event, color: Colors.white,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationCheck())
                      );
                    },
                    icon: Icon(Icons.content_paste_search_outlined)
                ),
              ],
            ),
          )
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            SizedBox(height: 10,),
            Image.asset('assets/vacation.gif'),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('휴가 제목', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderTextField(
                name: 'vacationName',
                decoration: const InputDecoration(
                  filled: true,
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('휴가 종류', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderDropdown<String>(
                name: 'vacationType',
                decoration: const InputDecoration(
                  filled: true,
                ),
                validator: FormBuilderValidators.compose(
                    [FormBuilderValidators.required(errorText: formErrorRequired)]),
                items: dropdownVacationType,
              ),
            ),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('휴가 신청일', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 10,),
            Container(
             margin: EdgeInsets.only(left: 30, right: 30),
             child: FormBuilderDateTimePicker(
               name: 'dateVacation',
               format: DateFormat('yyyy-MM-dd'),
               inputType: InputType.date,
               decoration: InputDecoration(
                 filled: true,
                 suffixIcon: IconButton(
                   icon: const Icon(Icons.close),
                   onPressed: () {
                     _formKey.currentState!.fields['dateVacation']?.didChange(null);
                   },
                 ),
               ),
               locale: const Locale.fromSubtags(languageCode: 'ko'),
               validator: FormBuilderValidators.compose(
                   [FormBuilderValidators.required(errorText: formErrorRequired)]
               ),
             ),
           ),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('휴가 사유', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 10,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderTextField(
                name: 'vacationReason',
                decoration: const InputDecoration(
                  filled: true,
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.maxLength(50, errorText: formErrorMaxLength(50)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(height: 30,),
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate()??false) {
                    VacationCreateRequest request = VacationCreateRequest(
                      _formKey.currentState!.fields['vacationName']!.value,
                      _formKey.currentState!.fields['vacationType']!.value,
                      DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['dateVacation']!.value),
                      _formKey.currentState!.fields['vacationReason']!.value,
                    );

                    _setVacation(request);
                  }
                },
                child: Text('신청하기')
            ),
            SizedBox(height: 20,),
          ],
        )
      ),
    );
  }
}
