import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_list_item.dart';
import 'package:app_company_employee_manager/pages/page_attendance.dart';
import 'package:app_company_employee_manager/pages/page_attendance_check.dart';
import 'package:app_company_employee_manager/pages/page_menu.dart';
import 'package:app_company_employee_manager/pages/page_my_info.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:flutter/material.dart';

class PageMenuList extends StatelessWidget {
  const PageMenuList({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
          title: "목록"
      ),
      body:Column(
        children: [
          ComponentListItem(
              menuListName: '메뉴',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageMenu())
                );
              }
          ),
          ComponentListItem(
              menuListName: '출퇴근 등록',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageAttendance())
                );
              }
          ),
          ComponentListItem(
              menuListName: '출퇴근 조회',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                );
              }
          ),
          ComponentListItem(
              menuListName: '휴가 신청',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageVacationApplication())
                );

              }
          ),
          ComponentListItem(
              menuListName: '휴가 조회',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageVacationCheck())
                );
              }
          ),
          ComponentListItem(
              menuListName: '마이페이지',
              callback: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => PageMyInfo())
                );
              }
          ),
        ],
      )
    );
  }
}
