import 'package:app_company_employee_manager/model/vacation/vacation_my_list_item.dart';

class VacationMyListItemResult {
  bool isSuccess;
  int code;
  String msg;
  int totalPage;
  int totalItemCount;
  int currentPage;
  List<VacationMyListItem> list;

  VacationMyListItemResult(this.isSuccess, this.code, this.msg, this.totalPage, this.totalItemCount, this.currentPage, this.list);

  factory VacationMyListItemResult.fromJson(Map<String, dynamic> json) {
    return VacationMyListItemResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      json['totalPage'],
      json['totalItemCount'],
      json['currentPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => VacationMyListItem.fromJson(e)).toList(),
    );
  }
}