class VacationMyListItem {
  String vacationName;
  String dateVacation;
  String vacationApproveSituation;

  VacationMyListItem(this.vacationName, this.dateVacation, this.vacationApproveSituation);

  factory VacationMyListItem.fromJson(Map<String, dynamic> json) {
    return VacationMyListItem(
      json['vacationName'],
      json['dateVacation'],
      json['vacationApproveSituation'],
    );
  }

}