class EmployeeLoginResponse {
  int employeeId;
  String employeeBirthday;
  int employeeNumber;
  String username;
  String dateIn;

  EmployeeLoginResponse(this.employeeId, this.employeeBirthday, this.employeeNumber, this.username, this.dateIn);

  factory EmployeeLoginResponse.fromJson(Map<String, dynamic> json) {
    return EmployeeLoginResponse(
      json['employeeId'],
      json['employeeBirthday'],
      json['employeeNumber'],
      json['username'],
      json['dateIn'],
    );
  }
}