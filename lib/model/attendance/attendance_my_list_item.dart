class AttendanceMyListItem {
  String dateReference;
  String timeCompanyIn;
  String timeCompanyOut;
  String timeEarlyLeave;
  String attendanceSituation;

  AttendanceMyListItem(this.dateReference, this.timeCompanyIn, this.timeCompanyOut, this.timeEarlyLeave, this.attendanceSituation);

  factory AttendanceMyListItem.fromJson(Map<String, dynamic> json) {
    return AttendanceMyListItem(
      json['dateReference'],
      json['timeCompanyIn'],
      json['timeCompanyOut'],
      json['timeEarlyLeave'],
      json['attendanceSituation'],
    );
  }
}