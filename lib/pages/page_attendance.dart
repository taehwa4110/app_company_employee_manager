import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/pages/page_attendance_check.dart';
import 'package:app_company_employee_manager/pages/page_menu.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:app_company_employee_manager/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';

class PageAttendance extends StatefulWidget {
  const PageAttendance({Key? key}) : super(key: key);

  @override
  State<PageAttendance> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendance> {
  var now = DateTime.now();
  String _attendanceSituation = '';
  String _timeCompanyIn = '-';
  String _timeCompanyOut = '-';
  String _timeEarlyLeave = '-';



  void initState() {
    super.initState();
    _getMyAttendance();
  }

  Future<void> _getMyAttendance() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getMySituation().then((res) {
      BotToast.closeAllLoading();
      setState(() {
        _attendanceSituation = res.data.attendanceSituation;
        _timeCompanyIn = res.data.timeCompanyIn;
        _timeCompanyOut = res.data.timeCompanyOut;
        _timeEarlyLeave = res.data.timeEarlyLeave;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

    });
  }

  Future<void> _setAttendance() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().setAttendance().then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '출근 등록 성공',
        subTitle: '출근이 정상적으로 등록되었습니다.',
      ).call();

      setState(() {
        if (res.isSuccess == true) {
          _getMyAttendance();
        }


      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '출근 등록 실패',
        subTitle: '출근 등록이 실패하였습니다. 다시 시도해주세요.',
      ).call();

    });
  }

  Future<void> _putAttendance(String attendanceSituation) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().putAttendanceSituation(attendanceSituation).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '근태 수정 성공',
        subTitle: '근태 상황이 성공적으로 수정되었습니다.',
      ).call();

      setState(() {
        _getMyAttendance();
      });

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '근태 수정 실패',
        subTitle: '근태 상황 수정이 실패하였습니다. 다시 시도해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
          title: '출 퇴근 등록'
      ),
      body:_buildBody(),
      bottomNavigationBar: BottomAppBar(
          color: Colors.blue,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageMenu())
                      );
                    },
                    icon: Icon(Icons.home,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendance())
                      );
                    },
                    icon: Icon(Icons.watch_later, color: Colors.white,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                      );
                    },
                    icon: Icon(Icons.calendar_today)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationApplication())
                      );
                    },
                    icon: Icon(Icons.event)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationCheck())
                      );
                    },
                    icon: Icon(Icons.content_paste_search_outlined)
                ),
              ],
            ),
          )
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child:  Column(
        children: [
          SizedBox(height: 20,),
          TimerBuilder.periodic(
            Duration(seconds: 1),
            builder: (context) {
              return Text(
                '${DateFormat('yyyy-MM-dd \nh:mm:ss a').format(DateTime.now())}',
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              );
            },
          ),
          SizedBox(height: 10,),
          Text('현재 나의 근태 상황 : $_attendanceSituation'),
          SizedBox(height: 20,),
          Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (_attendanceSituation == '')
                  Column(
                    children: [
                      Image.asset('assets/attendance.png'),
                      GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(top: 50,left: 20, right: 30),
                          width: 150,
                          height: 150,
                          decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.indigoAccent),
                          child: Text('출근', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, height: 3, fontSize: 30),),
                        ),
                        onTap: () {
                          //Todo 팝업창 추가 필요(연동하면서)
                          _setAttendance();
                        },
                      ),
                    ],
                  ),
                if(_attendanceSituation == '출근')
                  Column(
                    children: [
                      Image.asset('assets/hardwork.gif', width: 400, height: 230,),
                      Row(
                        children: [
                          GestureDetector(
                            child: Container(
                              margin: EdgeInsets.only(top: 50,left: 20, right: 30),
                              width: 150,
                              height: 150,
                              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.indigoAccent),
                              child: Text('조퇴',textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, height: 3, fontSize: 30),),
                            ),
                            onTap: () {
                              //Todo 팝업창 추가 필요(연동하면서)
                              _putAttendance('EARLY_LEAVE');
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              margin: EdgeInsets.only(top: 50,left: 20, right: 30),
                              width: 150,
                              height: 150,
                              decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.indigoAccent),
                              child: Text('퇴근',textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, height: 3, fontSize: 30),),
                            ),
                            onTap: () {
                              //Todo 팝업창 추가 필요(연동하면서)
                              _putAttendance('COMPANY_OUT');
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                if(_attendanceSituation == '퇴근' || _attendanceSituation == '조퇴')
                  Image.asset('assets/free.png', width: 300,height: 300,),
              ],
            ),
          ),
          SizedBox(height: 50,),
          DataTable(
              decoration:BoxDecoration(
                  border: Border.all(color: Colors.black)
              ) ,
              columns: [
                DataColumn(label: Text('출근 시간')),
                DataColumn(label: Text('조퇴 시간')),
                DataColumn(label: Text('퇴근 시간')),
              ],
              rows: [
                DataRow(
                    cells: [
                      DataCell(Text(_timeCompanyIn)),
                      DataCell(Text(_timeEarlyLeave)),
                      DataCell(Text(_timeCompanyOut)),
                    ]
                )
              ]
          ),
          SizedBox(height: 20,),
        ],
      ),
    );
  }
}
