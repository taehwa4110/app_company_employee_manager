import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/config/config_form_validator.dart';
import 'package:app_company_employee_manager/model/employee/employee_password_change_request.dart';
import 'package:app_company_employee_manager/pages/page_login.dart';
import 'package:app_company_employee_manager/repository/repo_employee.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PagePasswordChange extends StatefulWidget {
  const PagePasswordChange({Key? key}) : super(key: key);

  @override
  State<PagePasswordChange> createState() => _PagePasswordChangeState();
}

class _PagePasswordChangeState extends State<PagePasswordChange> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _putPasswordChange(EmployeePasswordChangeRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoEmployee().putPasswordChange(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '비밀번호 교체 성공',
          subTitle: '비밀번호 교체에 성공하였습니다. 다시 로그인해주세요'
      ).call();
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '비밀번호 교체 실패',
          subTitle: '비밀번호 교체에 실패하였습니다'
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
          title: '비밀번호 변경'
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            Image.asset('assets/password.jpg', height: 400,),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('기존 비밀번호', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 5,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderTextField(
                name: 'currentPassword',
                decoration: const InputDecoration(
                  labelText: '기존 비밀번호를 입력해주세요.',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('변경할 비밀번호', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 5,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderTextField(
                name: 'changePassword',
                decoration: const InputDecoration(
                  labelText: '변경할 비밀번호를 입력해주세요.',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(height: 20,),
            Container(
              margin: EdgeInsets.only(left: 30),
              alignment: Alignment.centerLeft,
              child: Text('비밀번호 재확인', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            SizedBox(height: 5,),
            Container(
              margin: EdgeInsets.only(left: 30, right: 30),
              child: FormBuilderTextField(
                name: 'checkChangePassword',
                decoration: const InputDecoration(
                  labelText: '변경할 비밀번호를 다시 한번 더 입력해주세요.',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                  FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                ]),
                keyboardType: TextInputType.text,
              ),
            ),
            SizedBox(height: 20,),
            OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate()??false) {
                    EmployeePasswordChangeRequest request = EmployeePasswordChangeRequest(
                      _formKey.currentState!.fields['currentPassword']!.value,
                      _formKey.currentState!.fields['changePassword']!.value,
                      _formKey.currentState!.fields['checkChangePassword']!.value,
                    );

                    _showChangeDialog(request);
                  }
                }, //TODO 확인버튼을 누르면 교체할것인지 여부를 물어보는 팝업창 필요
                child: Text('확인')
            ),
            SizedBox(height: 30,),
          ],
        ),
      ),
    );
  }

  void _showChangeDialog(EmployeePasswordChangeRequest request) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('비밀번호 교체'),
            content: Text('정말 비밀번호를 교체하시겠습니까?'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    _putPasswordChange(request);
                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => PageLogin()), (route) => false);
                  },
                  child: Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('취소')
              )
            ],
          );
        }
    );
  }
}
