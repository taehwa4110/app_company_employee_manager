class VacationListItem {
  int id;
  String vacationName;
  String vacationType;
  DateTime dateVacation;
  String vacationReason;


  VacationListItem(
      this.id,
      this.vacationName,
      this.vacationType,
      this.dateVacation,
      this.vacationReason,
      );

  factory VacationListItem.fromJson(Map<String, dynamic> json) {
    return VacationListItem(
      json['id'],
      json['vacationName'],
      json['vacationType'],
      DateTime.parse(json['dateHoliday']),
      json['vacationReason'],
    );
  }
}
