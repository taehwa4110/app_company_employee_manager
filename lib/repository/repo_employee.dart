import 'package:app_company_employee_manager/config/config_api.dart';
import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/model/common_result.dart';
import 'package:app_company_employee_manager/model/employee/employee_detail_result.dart';
import 'package:app_company_employee_manager/model/employee/employee_login_request.dart';
import 'package:app_company_employee_manager/model/employee/employee_login_single_result.dart';
import 'package:app_company_employee_manager/model/employee/employee_password_change_request.dart';
import 'package:dio/dio.dart';

class RepoEmployee {
  Future<EmployeeLoginSingleResult> doLogin(EmployeeLoginRequest loginRequest) async {
    String baseUrl = '$apiUri/employee/login';
    var dataTest = loginRequest.toJson();

    Dio dio = Dio();

    final response = await dio.post(
      baseUrl,
      data: loginRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return EmployeeLoginSingleResult.fromJson(response.data);
  }

  Future<EmployeeDetailResult> getEmployeeDetail() async {
    String baseUrl = '$apiUri/employee/{employeeId}';

    Dio dio = Dio();

    String? token =  await TokenLib.getToken();

    final response = await dio.get(
        baseUrl.replaceAll('{employeeId}', token!),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return EmployeeDetailResult.fromJson(response.data);
  }

  Future<CommonResult> putPasswordChange(EmployeePasswordChangeRequest request) async {
    String baseUrl = '$apiUri/employee/employee-password-change/{employeeId}';
    var test = request.toJson();

    Dio dio = Dio();

    String? token = await TokenLib.getToken();

    final response = await dio.put(
      baseUrl.replaceAll('{employeeId}', token!),
      data: request.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return CommonResult.fromJson(response.data);
  }
}