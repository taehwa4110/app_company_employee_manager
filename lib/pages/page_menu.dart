import 'package:app_company_employee_manager/components/component_appbar_actions.dart';
import 'package:app_company_employee_manager/components/component_menu_list.dart';
import 'package:app_company_employee_manager/config/config_color.dart';
import 'package:app_company_employee_manager/pages/page_attendance.dart';
import 'package:app_company_employee_manager/pages/page_attendance_check.dart';
import 'package:app_company_employee_manager/pages/page_my_info.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:flutter/material.dart';

class PageMenu extends StatelessWidget {
  const PageMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarActions(
        title: '메뉴',
        isUseActionBtn2: true,
        action2Icon: Icons.account_circle_outlined,
        action2Callback: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PageMyInfo())
          );
        },
      ),
      body:_buildBody(context),
      bottomNavigationBar: BottomAppBar(
        color: Colors.blue,
        child: Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageMenu())
                    );

                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.home, color: Colors.white,)
              ),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageAttendance())
                    );

                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.watch_later)
              ),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                    );

                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.calendar_today)
              ),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageVacationApplication())
                    );

                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.event)
              ),
              IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageVacationCheck())
                    );
                  },
                  icon: Icon(Icons.content_paste_search_outlined)
              ),
            ],
          ),
        )
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset('assets/company.jpg', width: 200,height: 200,),
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(top: 100,left: 30, right: 50,bottom: 70),
                alignment: Alignment.center,
                width: 150,
                height: 150,
                decoration:  BoxDecoration(
                    border: Border.all(color: Colors.black, width: 3),
                    color: Colors.white
                ),
                child: ComponentMenuList(
                  menuName: '출퇴근 등록',
                  icon: Icons.watch_later,
                  callback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageAttendance())
                    );
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 100, bottom: 70),
                alignment: Alignment.center,
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 3),
                    color: Colors.white
                ),
                child: ComponentMenuList(
                  menuName: '출퇴근 조회',
                  icon: Icons.calendar_today,
                  callback: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                    );
                  },
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                  margin: EdgeInsets.only(left: 30, right: 50),
                  alignment: Alignment.center,
                  width: 150,
                  height: 150,
                  decoration:  BoxDecoration(
                      border: Border.all(color: Colors.black, width: 3),
                      color: Colors.white
                  ),
                  child: ComponentMenuList(
                    menuName: '휴가신청',
                    icon: Icons.event,
                    callback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationApplication())
                      );
                    },
                  )
              ),
              Container(
                  alignment: Alignment.center,
                  width: 150,
                  height: 150,
                  decoration:  BoxDecoration(
                      border: Border.all(color: Colors.black, width: 3),
                      color: Colors.white
                  ),
                  child: ComponentMenuList(
                    menuName: '휴가 조회',
                    icon: Icons.content_paste_search_outlined,
                    callback: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationCheck())
                      );
                    },
                  )
              ),
            ],
          )
        ],
      )
    );
  }
}
