class VacationCountDetail {
  int employeeId;
  String totalVacationCount;
  String usageVacationCount;
  String leaveVacationCount;
  String dateVacationCountStart;
  String dateVacationCountEnd;

  VacationCountDetail(this.employeeId, this.totalVacationCount, this.usageVacationCount, this.leaveVacationCount, this.dateVacationCountStart, this.dateVacationCountEnd);

  factory VacationCountDetail.fromJson(Map<String, dynamic> json) {
    return VacationCountDetail(
      json['employeeId'],
      json['totalVacationCount'],
      json['usageVacationCount'],
      json['leaveVacationCount'],
      json['dateVacationCountStart'],
      json['dateVacationCountEnd'],
    );
  }
}