import 'package:app_company_employee_manager/model/attendance/attendance_my_list_item.dart';
import 'package:flutter/material.dart';

class ComponentAttendanceList extends StatelessWidget {
  const ComponentAttendanceList({super.key, required this.item});
  final AttendanceMyListItem item;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 70,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(item.dateReference),
              Column(
                children: [
                  Text('출근시간: ${item.timeCompanyIn}'),
                  Text('조퇴시간: ${item.timeEarlyLeave}'),
                  Text('퇴근시간: ${item.timeCompanyOut}'),
                ],
              ),
              Text(item.attendanceSituation),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
