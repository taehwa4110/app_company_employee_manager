import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/pages/page_login.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  static Future<String?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  static void setToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', token);
  }

  static void logout(BuildContext context) async {
    BotToast.closeAllLoading();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');

    ComponentNotification(
        success: false,
        title: '로그아웃',
        subTitle: '로그아웃되어 로그인 페이지로 이동합니다.'
    );
    
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => const PageLogin()), (route) => false);
  }
}