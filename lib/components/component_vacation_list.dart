import 'package:app_company_employee_manager/model/vacation/vacation_my_list_item.dart';
import 'package:flutter/material.dart';

class ComponentVacationList extends StatelessWidget {
  const ComponentVacationList({super.key, required this.item});
  final VacationMyListItem item;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: const Color.fromRGBO(100, 100, 100, 100))
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(item.dateVacation),
                Text(item.vacationName),
                Text(item.vacationApproveSituation),
              ],
            )
        ),
        SizedBox(height: 10,),
      ],
    );
  }
}
