import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_attendance_list.dart';
import 'package:app_company_employee_manager/components/component_count_title.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/config/config_dropdown.dart';
import 'package:app_company_employee_manager/model/attendance/attendance_my_list_item.dart';
import 'package:app_company_employee_manager/pages/page_attendance.dart';
import 'package:app_company_employee_manager/pages/page_menu.dart';
import 'package:app_company_employee_manager/pages/page_vacation_application.dart';
import 'package:app_company_employee_manager/pages/page_vacation_check.dart';
import 'package:app_company_employee_manager/repository/repo_attendance.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class PageAttendanceCheck extends StatefulWidget {
  const PageAttendanceCheck({Key? key}) : super(key: key);

  @override
  State<PageAttendanceCheck> createState() => _PageAttendanceCheckState();
}

class _PageAttendanceCheckState extends State<PageAttendanceCheck> {
  var now = DateTime.now();
  int _selectYear = DateTime.now().year;
  int _selectMonth = DateTime.now().month;


  int day = DateTime.monthsPerYear;
  final _scrollController = ScrollController();

  List<AttendanceMyListItem> _list = [];

  void initState() {
    super.initState();
    _getMyAttendanceList();
  }

  Future<void> _getMyAttendanceList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAttendance().getMyList(_selectYear, _selectMonth).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '출퇴근 조회',
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody()
        ],
      ),
      bottomNavigationBar: BottomAppBar(
          color: Colors.blue,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageMenu())
                      );
                    },
                    icon: Icon(Icons.home,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendance())
                      );
                    },
                    icon: Icon(Icons.watch_later)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageAttendanceCheck())
                      );
                    },
                    icon: Icon(Icons.calendar_today, color: Colors.white,)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationApplication())
                      );
                    },
                    icon: Icon(Icons.event)
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => PageVacationCheck())
                      );
                    },
                    icon: Icon(Icons.content_paste_search_outlined)
                ),
              ],
            ),
          )
      ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 10,),
          _buildForm(),
          SizedBox(height: 30,),
          Divider(),
          ComponentCountTitle(icon: Icons.list, count: _list.length, unitName: '건', itemName: '데이터'),
          Divider(),
          ListView.builder(
            padding: const EdgeInsets.all(8),
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) =>
              ComponentAttendanceList(item: _list[index])),
        ],
      ),
    );
  }

  Widget _buildForm() {
    return SingleChildScrollView(
      child: Row(
        children: [
          DropdownButtonHideUnderline(
            child: DropdownButton2(
              isExpanded: true,
              hint: Row(
                children: [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: Colors.brown,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      _selectYear.toString(),
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
              items: dropdownYear,
              onChanged: (year) {
                setState(() {
                  _selectYear = year!;
                  _getMyAttendanceList();
                });
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              iconSize: 14,
              iconEnabledColor: Colors.indigo,
              iconDisabledColor: Colors.grey,
              buttonHeight: 50,
              buttonWidth: 160,
              buttonPadding: const EdgeInsets.only(left: 14, right: 14),
              buttonDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                border: Border.all(
                  color: Colors.black26,
                ),
                color: Colors.blueAccent,
              ),
              buttonElevation: 2,
              itemHeight: 40,
              itemPadding: const EdgeInsets.only(left: 14, right: 14),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownPadding: null,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: Colors.blueAccent,
              ),
              dropdownElevation: 8,
              scrollbarRadius: const Radius.circular(40),
              scrollbarThickness: 6,
              scrollbarAlwaysShow: true,
              offset: const Offset(-20, 0),
            ),
          ),
          Text('년'),
          SizedBox(width: 20,),
          DropdownButtonHideUnderline(
            child: DropdownButton2(
              isExpanded: true,
              hint: Row(
                children: [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: Colors.brown,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      _selectMonth.toString(),
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
              items: dropdownMonth,
              onChanged: (month) {
                setState(() {
                  _selectMonth = month!;
                  _getMyAttendanceList();
                });
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
              ),
              iconSize: 14,
              iconEnabledColor: Colors.indigo,
              iconDisabledColor: Colors.grey,
              buttonHeight: 50,
              buttonWidth: 160,
              buttonPadding: const EdgeInsets.only(left: 14, right: 14),
              buttonDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                border: Border.all(
                  color: Colors.black26,
                ),
                color: Colors.blueAccent,
              ),
              buttonElevation: 2,
              itemHeight: 40,
              itemPadding: const EdgeInsets.only(left: 14, right: 14),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownPadding: null,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: Colors.blueAccent,
              ),
              dropdownElevation: 8,
              scrollbarRadius: const Radius.circular(40),
              scrollbarThickness: 6,
              scrollbarAlwaysShow: true,
              offset: const Offset(-20, 0),
            ),
          ),
          Text('월'),
        ],
      ),

    );
  }
}


