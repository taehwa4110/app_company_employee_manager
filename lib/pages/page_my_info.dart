import 'package:app_company_employee_manager/components/component_appbar_popup.dart';
import 'package:app_company_employee_manager/components/component_custom_loading.dart';
import 'package:app_company_employee_manager/components/component_my_info.dart';
import 'package:app_company_employee_manager/components/component_notification.dart';
import 'package:app_company_employee_manager/functions/token_lib.dart';
import 'package:app_company_employee_manager/model/employee/employee_detail.dart';
import 'package:app_company_employee_manager/pages/page_login.dart';
import 'package:app_company_employee_manager/pages/page_password_change.dart';
import 'package:app_company_employee_manager/repository/repo_employee.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageMyInfo extends StatefulWidget {
  const PageMyInfo({Key? key}) : super(key: key);

  @override
  State<PageMyInfo> createState() => _PageMyInfoState();
}

class _PageMyInfoState extends State<PageMyInfo> {
  EmployeeDetail? _employeeDetail;

  void initState() {
    super.initState();
    _getMyInfo();
  }

  Future<void> _getMyInfo() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc){
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoEmployee().getEmployeeDetail().then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: true,
          title: '마이페이지 업로드 성공',
          subTitle: res.msg
      );
      setState(() {
        _employeeDetail = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
          success: false,
          title: '마이페이지 업로드 실패',
          subTitle: "마이페이지 업로드에 실패하였습니다."
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: '마이페이지'
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 30,),
            ComponentMyInfo(myInfoKind: '이름', contents: _employeeDetail?.employeeName ?? ' - '),
            ComponentMyInfo(myInfoKind: '사원번호', contents: _employeeDetail?.employeeNumber.toString() ?? ' - '),
            ComponentMyInfo(myInfoKind: '아이디', contents: _employeeDetail?.username ?? ' - '),
            ComponentMyInfo(myInfoKind: '생년월일', contents: _employeeDetail?.employeeBirthday ?? ' - '),
            ComponentMyInfo(myInfoKind: '분야', contents: _employeeDetail?.employeeField ?? ' - '),
            ComponentMyInfo(myInfoKind: '직급', contents: _employeeDetail?.employeeGrade ?? ' - '),
            ComponentMyInfo(myInfoKind: '입사일', contents: _employeeDetail?.dateIn ?? ' - '),
            SizedBox(height: 30,),
            OutlinedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PagePasswordChange())
                  );
                },
                child: const Text('비밀번호 변경')
            ),
            SizedBox(height: 20,),
            OutlinedButton(onPressed: _showLogoutDialog, child: Text('로그아웃'))
          ],
        ),
      )
    );
  }

  void _showLogoutDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('로그아웃'),
            content: Text('정말 로그아웃 하시겠습니까?'),
            actions: [
              OutlinedButton(
                  onPressed: () {
                    TokenLib.logout(context);
                  },
                  child: Text('확인')
              ),
              OutlinedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('취소')
              )
            ],
          );
        }
    );
  }
}
